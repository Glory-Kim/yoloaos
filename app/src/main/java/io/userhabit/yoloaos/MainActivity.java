package io.userhabit.yoloaos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import io.userhabit.yoloaos.ui.MainFragment;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomNavigationView;
    View menuView;
    int tab_position = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                if(item.getItemId() == R.id.nav_menu1){
                    tab_position = 0;
                }
//                else if(item.getItemId() == R.id.nav_menu2){
//                    tab_position = 1;
//                }else if(item.getItemId() == R.id.nav_menu3){
//                    tab_position = 2;
//                }else if(item.getItemId() == R.id.nav_menu4){
//                    tab_position = 3;
//                }

                moveTabPage(tab_position);
                return true;
            }
        });
    }

    void init(){
        bottomNavigationView = findViewById(R.id.bnv_main);
        menuView = findViewById(R.id.menu_view);
        ImageView btnAdd =  menuView.findViewById(R.id.imageView2);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity( new Intent(getApplicationContext(), WriteSceneActivity.class));
            }
        });

        moveTabPage(0);
    }
    //실제적으로 페이지 이동을 해주는 메소드
    public void moveTabPage(int position){
//        pageLoadingStart();
        switch (position){
            case 0:
                refreshTab(new MainFragment(),true);
                break;
        }
    }
    // 새로고침 하거나 페이지 이동시 사용되는 메소드
    public void refreshTab(Fragment f, boolean reload){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        if (reload) {
            getSupportFragmentManager().popBackStack();
        }
        transaction.replace(R.id.fl_content_main,f);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        /**
         * 데이터 통신 코드
         * */
    }
}
