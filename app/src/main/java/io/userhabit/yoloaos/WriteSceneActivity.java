package io.userhabit.yoloaos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import io.userhabit.yoloaos.ui.decoration.CommonDecoration;

public class WriteSceneActivity extends AppCompatActivity {

    View bodyView,bottomView;
    RecyclerView recyclerView;
    Button btn_ok,btn_cancel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wirte_scene);
        init();
    }

    void init(){

        bodyView = findViewById(R.id.include_layout_body);
        recyclerView = bodyView.findViewById(R.id.recycler);

        bottomView = findViewById(R.id.include_layout_bottom);
        btn_ok = bottomView.findViewById(R.id.btn_ok);
        btn_cancel = bottomView.findViewById(R.id.btn_cancel);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new CommonDecoration(8));

    }
}