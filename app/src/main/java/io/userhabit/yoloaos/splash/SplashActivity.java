package io.userhabit.yoloaos.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import io.userhabit.yoloaos.MainActivity;
import io.userhabit.yoloaos.R;

public class SplashActivity extends Activity {

    boolean autoLogin = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread timer = new Thread(){
            @Override
            public void run() {
                try{
//                    Userhabit.setScreen(Splash.this,"스플래쉬 화면");
                    sleep(2000);
                }catch (Exception e){

                }finally {
//                    if(autoLogin)
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    SplashActivity.this.finish();
                }
            }
        };
        timer.start();
    }

    @Override
    protected void onStart() {
        super.onStart();
        autoLogin = isAutoLogin();
        /**
         * 회원정보 받아오는 코드
         * */
    }

    boolean isAutoLogin(){
        boolean result = false;
        /**
         * 자동로그인 판별코드
         * */
        return result;
    }

    @Override
    public void onBackPressed() {
    }
}
