package io.userhabit.yoloaos.ui.decoration

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class CommonDecoration(divHeight:Int) : RecyclerView.ItemDecoration() {

    private var divHeight = divHeight

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)

        var itemCount = parent.adapter?.itemCount

        if(parent.getChildAdapterPosition(view) == 0){
            outRect.left = divHeight
            outRect.right = divHeight
            outRect.top = divHeight

        }else{
            outRect.bottom = divHeight
            outRect.left = divHeight
            outRect.right = divHeight
            outRect.top = divHeight
        }


    }
}