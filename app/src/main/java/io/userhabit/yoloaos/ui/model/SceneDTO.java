package io.userhabit.yoloaos.ui.model;

import com.google.gson.annotations.SerializedName;

public class SceneDTO {

    /**
     * nickname
     * profile
     * content
     * content_img
     * scene_id
     * date
     * */

    @SerializedName("nickname")
    public String nickname = "";

    @SerializedName("profile")
    public String profile = "";

    @SerializedName("content")
    public String content = "";

    @SerializedName("content_img")
    public String content_img = "";

    @SerializedName("scene_id")
    public int scene_id = -1;

    @SerializedName("date")
    public String date = "";



}
