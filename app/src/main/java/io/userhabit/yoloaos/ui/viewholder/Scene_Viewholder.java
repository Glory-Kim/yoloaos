package io.userhabit.yoloaos.ui.viewholder;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
/**
 * layout item_scene.xml
 * */
public class Scene_Viewholder extends RecyclerView.ViewHolder {
    /**
     * public 으로 선언된 각 요소
     * */

    int position = -1;

    public Scene_Viewholder(@NonNull View itemView) {
        super(itemView);
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
